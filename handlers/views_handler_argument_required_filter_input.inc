<?php

class views_handler_argument_required_filter_input extends views_handler_argument {
  public static $isEmpty = TRUE;

  function option_definition() {
    $options = parent::option_definition();
    $options['use_empty'] = array(
      'bool' => true,
      'default' => false,
    );
    return $options;
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
  }

  function options_form(&$form, &$form_State) {
    //parent::options_form($form, $form_State);
    $form['use_empty'] = array(
      '#type' => 'checkbox',
      '#title' => t('Count empty strings, false, or the number 0 as empty?'),
      '#description'=> t('Uses PHP\'s "empty" instead of only checking for null.'),
      '#return_value' => true,
      '#default_value' => $this->options['use_empty'],
    );
  }

  /**
   * @param null $which
   * @param bool|FALSE $reset Reset static caching. Useful for unit testing.
   * @return bool
   */
  function default_action($which = null, $reset = false) {
    static $cache = NULL;
    if (!isset($cache) || $reset) {
      $view = $this->view;
      if (is_array($view->filter) && count($view->filter)) {
        foreach ($view->filter as $filter_id => $filter) {
          if ($filter->is_exposed()) {
            $identifier = $filter->options['expose']['identifier'];
            if (isset($view->exposed_input[$identifier])
                && !$this->isFilterEmpty($view->exposed_input[$identifier])) {

              $cache = TRUE;
              self::$isEmpty = (self::$isEmpty && !$cache);
              return $cache;
            }
          }
        }
      }
      $cache = FALSE;
    }
    return $cache;
  }

  private function isFilterEmpty($filter) {
    if(is_array($filter)) {
      foreach($filter as $value) {
        return $this->isFilterEmpty($value);
      }
    }
    else {
      return (strlen($filter) == 0
              || ($this->options['use_empty']
                  && empty($filter)));
    }
  }

//  /**
//   * Checks if the current filters are all empty.
//   * @param bool|FALSE $reset Allow manual static cache reset.
//   * @return bool Return true if empty.
//   */
//  function areFiltersEmpty($reset = false) {
//    static $cache = null;
//    if(!isset($cache) || $reset) {
//      $cache = true;
//      foreach($this->filter as $key => $filter) {
//        $cache = !empty($filter->value);
//        if(!$cache) {
//          return $cache;
//        }
//      }
//    }
//    return $cache;
//  }

  function validate_argument_basic($arg) {
    return parent::validate_argument_basic($arg);
  }

  function query($group_by = false) {
    parent::query($group_by);
  }
}
